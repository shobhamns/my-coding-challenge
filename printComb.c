 /************************************************************************
  * Program	:	printComb.c                                         *
  *                                                                      *  
  * Description	:                                                        *
  *    C Program to display all possible combinations of                 *
  *    integers in ascending order that sum up to the given Number.      *
  *                                                                      *
  * Parameters:                                                          *
  *    Interactively accepts a Positive integer for which the            *
  *    different comnimations are displayed.                             *
  ************************************************************************/

//Include some files
#include <stdio.h>
#include <stdlib.h>

 /************************************************************************   
  * Function: pintDiffComb                                               *
  *                                                                      * 
  * Description:                                                         *
  *    Recursive function that displays all possible combinations of     *
  *    integers in ascending order that sum up to the given number 'n'   *
  *                                                                      *
  * Parameters:                                                          *
  *                                                                      *
  *    n	: Positive integer for which the different               *
  *        	  combinations are displayed as per rule.                *
  *    arr	: Array to temporarily hold the different combinations   *
  *    i	: Index in the Array starting from which the             *
  *               number is blown up to its respective combination.      *
  *                                                                      *  
  * Returns	: None.                                                  *
  ************************************************************************/   
void printDiffComb(int n, int *arr, int i)
{
	int mid 	= n/2; //Middle value of the given number n
	int left 	= 1;   //Left part of the split number 

	int index 	= 0;   //Array index tracker
	
	//Function exits when n approaches 0
	if (!n) {
		printf("\n");
		return;
	}

	//Loop that splits the number and prints combinations
	for (left = 1; left <= mid; left++) {

		//Check to make sure the combination is in ascending order	
		if (left < arr[i-1]) 
			continue;

		arr[i++] = left;
		arr[i] = n-left;
	
		//Print the combination stored in array
		printf("[");

		for (index = 0; index <= i; index++)
			printf(" %d", arr[index]);
		
		printf(" ]\n");

		/*Call the same function recursively 
                  to get the different combinations for the
                  Split number.*/ 
		if (left <= n-left)
			printDiffComb(n-left, arr, i);

		i--;
	}
}


 /************************************************************************
  * Function	: main                                                   *
  *                                                                      *
  * Description	:                                                        *  
  *                                                                      *
  *    Enty point to Program to display all possible combinations of     *
  *    integers in ascending order that sum up to the given number.      *
  *    Accepts input from user, validates it and calls                   *
  *    routine to display combinations.                                  * 
  *                                                                      *
  ************************************************************************/
void main()
{
	int n; //Number for which combinations are to be printed.
	int *arr = NULL; //Dynamic array to store different combinations.

	//Accept the input parameter from user.
	printf("Enter a non negative positive integer: \n");
	scanf("%d", &n);
	
	//Input validation.
	if (n < 1) {
		printf("Invalid value entered for inpiut.\n");
		return;
	}
	
	//Dynamically allocate memory as per given input.
	arr = (int *) malloc( sizeof(int) * n);
	if (!arr) {
		printf("Memory allocation failed.\n");
		return;
	}

	printf("[ %d ]\n", n);

	//Display different combinations.
	printDiffComb(n, arr, 0);

	//Free up allocated memory.
	if (arr) {
		free(arr);
		arr = NULL;
	}

	return;
}			
