------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in  the subject line. (e.g. 'John Public - DevOps Engineer: Interview Challenge Submission') 

Please don't hesitate to contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with, but typically for a DevOps/SysOps/SysAdmin position, we are expecting: Bash, Perl, Python, PowerShell, C, or even C++, but as long as we can execute it and see the results, it's fine with us.

For this homework challenge, you can choose between completing #1 or #2. Challenge #3 is mandatory for all submissions.

    1. Write a program/script that does the following:
        - Write a program/script that writes a program/script that writes itself.
        - Write a simple program/script that can be executed in more then one language with no changes.
        
    2. Submit a code sample of a tool or automated task you've written with an explanation of its intended use. The example should:
        - Have instructions of how to execute it for us to review.
        - Be written in any language, but preferably written in a language most DevOps/SysOps/SysAdmins would use regularly.
        - Be no more than 1,000 lines of code.

    3. Submit a copy of your .profile, .bashrc, .bash_profile, .bash_logout; for Windows users your profile.ps1 will do:
        - This submission may come from a personal environment.
        - This submission may come from a work environment (use best judgement).