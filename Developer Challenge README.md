------------------------------- Challenge instructions -------------------------------

Once you complete this exercise, please email the submission to jobs@theta-llc.com with your name, a hyphen, the position you're interviewing for, a colon, and 'Interview Challenge Submission' for in  the subject line. (e.g. 'John Public - Software Application Developer: Interview Challenge Submission').

Please don't hesitate contact your interviewer if you have any questions or feedback about the test. We're looking for cleverness and attention to detail, not how quickly you can get it done.

Please use whichever language you're comfortable with. As a developer, you're expected to have worked with a plethora of different programming languages, so represent yourself with your strongest language choice. As long as you can provide instructions on how to execute it so we can see the results, it's fine with us.

For this homework challenge, you can choose between completing #1 or #2

    1. Write a code that prints all combinations of positive integers in increasing order that sum to a given positive number:
            (For example, the program would take an input of and prints out:
            [1 1 1 1 1]
            [1 1 1 2]
            [1 1 3]
            [1 2 2]
            [1 4]
            [2 3]
            [5] 
            The elements need to be integers, not strings. 
            If the language you choose has a built-in function that accomplishes this task, try to avoid using it. 
            Additionally, n cannot equal 0 for the sake of this challenge ;))
        
    2. Submit a code sample of something you've written with an explanation of its intended use. The example should:
        - Have instructions of how to execute it for us to review.
        - Be written in any language. Showcase your best.
        - Be no more than 1,000 lines of code.